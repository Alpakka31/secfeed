# Secfeed

A program written in Python to read security feeds  

## Requirements
- Python >= 3.11 (tested on Python 3.11)  
- sty (https://pypi.org/project/sty/)  
- BeautifulSoup (https://pypi.org/project/beautifulsoup4/)  
- feedparser (https://pypi.org/project/feedparser/)  


