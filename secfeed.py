#!/usr/bin/python3
import feedparser
import sys
from bs4 import BeautifulSoup
from sty import fg, ef, rs

debian_dsa_feed_url="https://www.debian.org/security/dsa"
debian_dsa_long_feed_url="https://www.debian.org/security/dsa-long"
debian_dla_feed_url="https://www.debian.org/lts/security/dla"
debian_dla_long_feed_url="https://www.debian.org/lts/security/dla-long"
gentoo_glsa_feed_url="https://security.gentoo.org/glsa/feed.rss"
fedora_39_security_feed_url="https://bodhi.fedoraproject.org/rss/updates/?type=security&page=1&releases=F39&status=stable"

def read_debian_dsa_feed():
    feed = feedparser.parse(debian_dsa_feed_url)
    print(f"{ef.bold}{fg.green}====DEBIAN DSA===={fg.rs}{ef.rs}")
    for entry in feed.entries:
        print(f"{ef.bold}{fg.green}Title:{fg.rs} {entry.title}{ef.rs}")
        print(f"{ef.bold}{fg.green}Link:{fg.rs} {entry.link}{ef.rs}")
        print("")

def read_debian_dsa_long_feed():
    feed = feedparser.parse(debian_dsa_long_feed_url)
    print(f"{ef.bold}{fg.green}====DEBIAN DSA LONG===={fg.rs}{ef.rs}")
    for entry in feed.entries:
        print(f"{ef.bold}{fg.green}Title:{fg.rs} {entry.title}{ef.rs}")
        print(f"{ef.bold}{fg.green}Link:{fg.rs} {entry.link}{ef.rs}")
        print(f"{ef.bold}{fg.green}Summary:{fg.rs} {BeautifulSoup(entry.summary, 'lxml').text}{ef.rs}")
        print("")

def read_debian_dla_feed():
    feed = feedparser.parse(debian_dla_feed_url)
    print(f"{ef.bold}{fg.green}====DEBIAN DLA===={fg.rs}{ef.rs}")
    for entry in feed.entries:
        print(f"{ef.bold}{fg.green}Title:{fg.rs} {entry.title}{ef.rs}")
        print(f"{ef.bold}{fg.green}Link:{fg.rs} {entry.link}{ef.rs}")
        print("")

def read_debian_dla_long_feed():
    feed = feedparser.parse(debian_dla_long_feed_url)
    print(f"{ef.bold}{fg.green}====DEBIAN DLA LONG===={fg.rs}{ef.rs}")
    for entry in feed.entries:
        print(f"{ef.bold}{fg.green}Title:{fg.rs} {entry.title}{ef.rs}")
        print(f"{ef.bold}{fg.green}Link:{fg.rs} {entry.link}{ef.rs}")
        print(f"{ef.bold}{fg.green}Summary:{fg.rs} {BeautifulSoup(entry.summary, 'lxml').text}{ef.rs}")
        print("")

def read_gentoo_glsa_feed():
    feed = feedparser.parse(gentoo_glsa_feed_url)
    print(f"{ef.bold}{fg.green}====GENTOO GLSA===={fg.rs}{ef.rs}")
    for entry in feed.entries:
        print(f"{ef.bold}{fg.green}Title:{fg.rs} {entry.title}{ef.rs}")
        print(f"{ef.bold}{fg.green}Link:{fg.rs} {entry.link}{ef.rs}")
        print("")

def read_fedora_39_security_feed():
    feed = feedparser.parse(fedora_39_security_feed_url)
    print(f"{ef.bold}{fg.green}====FEDORA 39 SECURITY===={fg.rs}{ef.rs}")
    for entry in feed.entries:
        print(f"{ef.bold}{fg.green}Title:{fg.rs} {entry.title}{ef.rs}")
        print(f"{ef.bold}{fg.green}Link:{fg.rs} {entry.link}{ef.rs}")
        print(f"{ef.bold}{fg.green}Summary:{fg.rs} {BeautifulSoup(entry.summary, 'lxml').text}{ef.rs}")
        print("")

def main():
    if len(sys.argv) > 1:
        feed = sys.argv[1]

        if feed == "DSA":
            read_debian_dsa_feed()
        elif feed == "DSA_LONG":
            read_debian_dsa_long_feed()
        elif feed == "DLA":
            read_debian_dla_feed()
        elif feed == "DLA_LONG":
            read_debian_dla_long_feed()
        elif feed == "GLSA":
            read_gentoo_glsa_feed()
        elif feed == "FEDORA39":
            read_fedora_39_security_feed()
        elif feed == "ALL":
            read_debian_dsa_feed()
            read_debian_dsa_long_feed()
            read_debian_dla_feed()
            read_debian_dla_long_feed()
            read_gentoo_glsa_feed()
            read_fedora_39_security_feed()
        else:
            print(f"Invalid feed: {feed}")
    else:
        print(f"{ef.bold}{fg.green}FEEDS{fg.rs}{ef.rs}")
        print(": DSA (Debian DSA)")
        print(": DSA_LONG (Debian DSA Long)")
        print(": DLA (Debian DLA)")
        print(": DLA_LONG (Debian DLA_LONG")
        print(": GLSA (Gentoo GLSA)")
        print(": FEDORA39 (Fedora 39 Security)")
        print(": ALL (Show every feed)")
        print("\nNo feed given...")

if __name__ == "__main__":
    main()
